#!/bin/bash

source ./.env

eval `ssh-agent`
ssh-add $SSH_CONFIG

git config --local user.name $USER_NAME
git config --local user.email $USER_MAIL

rm -rf public

zola build --output-dir=public

git add public
git commit -m "build: GitLab pages"

git push origin HEAD
