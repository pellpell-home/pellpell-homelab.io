// static/js/set_ogps.js

async function setOGPs(ogpUrl, id) {
    let postFlag = false;

    // value は attribute, '' の場合は textContent
    const dataNames = {
        'image': 'src',
        'title': '',
        'domain': '',
        'description': '',
        'url': 'href'
    }

    // OGP データを POST 通信して取得するかどうか
    for (const name in dataNames) {
        if (!sessionStorage.getItem(`${id}-${name}`)) {
            postFlag = true;
            break;
        }
    }

    if (postFlag) {
        console.log("post!");

        await postData('https://api.pellpell.net/get_ogps', { url: ogpUrl })
            .then(data => {
                for (const name in dataNames) {
                    sessionStorage.setItem(`${id}-${name}`, data[name]);
                }
            });
    }

    // OGP をセット
    for (const name in dataNames) {
        setOGPValue(id, name, dataNames[name]);
    }
}

function setOGPValue(id, key, attribute) {
    const value = sessionStorage.getItem(`${id}-${key}`);
    if (!value) return;

    if (attribute === '') {
        document.getElementById(id).querySelector(`.ogp-${key}`).textContent = value;
    } else {
        document.getElementById(id).querySelector(`.ogp-${key}`).setAttribute(attribute, value);
    }
}
