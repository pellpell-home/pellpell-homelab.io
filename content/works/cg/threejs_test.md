+++
title = "Three.jsのテスト"
date = 2022-07-17
[taxonomies]
tags = ["Three.js", "CG"]
[extra]
url = "https://pellpell.gitlab.io/threejs_test/index.html"
home_url = "https://gitlab.com/pellpell/threejs_test/"
ogp_image = "https://pellpell.gitlab.io/threejs_test/assets/image/ogp_image_v2.png"
ogp_description = "Three.jsでBlenderの3Dモデルを読み込むテストページを作成しました"
use_post = true
read_ogp = true
use_iframe = true
width = 1350
height = 960
+++

<p><a href="https://gitlab.com/pellpell/threejs_test/" target="_blank" rel="noopener">Git リポジトリ</a></p>
<div id="reference">参考サイト:
    <a href="https://youtu.be/S6aAvxUx2ko" target="_blank"
            rel="noopener">3Dモデル</a>
    <a href="https://web-creates.com/code/blender-threejs/" target="_blank"
        rel="noopener">Webページ</a>
</div>
