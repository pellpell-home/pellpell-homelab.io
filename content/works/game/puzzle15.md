+++
title = "Puzzle15"
date = 2022-06-25
[taxonomies]
tags = ["rust", "bevy"]
[extra]
url = "https://pellpell.gitlab.io/puzzle15/"
home_url = "https://gitlab.com/pellpell/puzzle15/"
ogp_image = "https://pellpell.net/assets/images/puzzle15_ogp_v3.png"
ogp_description = "puzzle15 game page by PellPell"
use_post = true
read_ogp = true
use_iframe = true
+++
