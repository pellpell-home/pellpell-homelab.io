// static/js/iframe.js

window.addEventListener('resize', (_) => {
    resizeIFrame();
});

function resizeIFrame() {
    const width = parseFloat(window.getComputedStyle(document.getElementById('container'), "").width);

    const iframeContainers = document.querySelectorAll(".iframe-container");

    iframeContainers.forEach(iframeContainer => {
        const iframe = iframeContainer.querySelector(".iframe");

        const scale = width / parseFloat(iframe.width);
        iframe.style.transform = `scale(${scale})`;

        iframeContainer.style.height = `${parseFloat(iframe.height) * scale}px`;
    });
}

resizeIFrame();
